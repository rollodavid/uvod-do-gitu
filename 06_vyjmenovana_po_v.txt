Mezinárodní vlak V _ ndobona v _ kolejil ve v _ chřici
nedaleko svratecké vodní nádrže V _ r. Vagony spadly do
vody, rozv _ řily hladinu a zvedly v _ sokou vlnu, která se
hnala až k V _ škovu. To byl pov _ k. V _ ly, které
v _ jimečně nev _ ly věnce ve své v _ le jako obv _ kle, ale
v _ peckovávaly s v _ drami v _ šně u ohně na pobřeží, se
rychle rozběhly k vodním v _ rům, ve kterých vlak klesal ke
dnu. V _ ly dokázaly chodit po vodě, stouply si nad místo
té v _ hrocené katastrofy a rozv _ nuly své dlouhé vlasy.