Na starém hradě u města Kroměříž od nepaměti žijí duchové.
V zimě, tedy v měsících, kdy v potemnělých místnostech nechodí osamělí turisté, ožívá tato pamětihodnost tajemně strašidelnou atmosférou.
V trůnním sále vyučuje zapomnětlivý bezhlavý rytíř zeměpis.
Téměř všechny mladé duchy výklad ohromně baví.
Tedy kromě Bartoloměje.
Ten si upřímně přeje být už pryč ze školy, být zaměstnaný ve dvousměnném provozu na nějaké pořádné pevnosti a dostávat každý měsíc odměnu do svého měšce.
Extrémně strašit při měsíci a proměňovat oněmělé turisty v měkkýše.
Svými triky krkolomně děsit kastelány, dokud jim nepovolí močový měchýř.
Ačkoli Bartoloměj ve školních hodinách jen tak nepřítomně sedí, jeho učitelé žijí v domnění, že poslouchá výklad.
Inu, každý duch by se měl umět tvářit naprosto duchapřítomně.